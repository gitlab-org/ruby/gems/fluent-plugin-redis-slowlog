require "spec_helper"

describe Fluent::Plugin::RedisSlowlogInput do
  let(:config) do
    { tag: "redis.slowlog" }
  end

  let(:plugin) { driver.instance }
  let(:fake_redis) { instance_double(Redis, ping: "PONG", quit: "OK", slowlog: []) }

  subject(:driver) do
    config_string = config.map { |key, value| "#{key} #{value}" }.join("\n")
    Fluent::Test::Driver::Input.new(described_class).configure(config_string)
  end

  def run(**options)
    driver.run({ timeout: 1 }.merge(options))
  end

  before do
    Fluent::Test.setup
  end

  context "when redis can't be reached" do
    it "raises an error" do
      expect { run }.to raise_error(Redis::CannotConnectError)
    end
  end

  context "when specifying redis connection attributes" do
    let(:config) do
      { tag: "redis.slowlog",
        url: "redis://:p4ssw0rd@10.0.1.1:6380/15",
        path: "/path/to/redis.sock",
        host: "localhost",
        port: 1234,
        password: "5iveL!fe",
        timeout: 2 }
    end

    it "delegates all of them to redis-rb" do
      # In ruby 2.5+ this could be config.slice(*[])
      redis_params = config.select { |name, _| [:url, :path, :host, :port, :password, :timeout].include?(name) }

      expect(Redis).to receive(:new).with(redis_params).and_return(fake_redis)

      run
    end
  end

  context "when redis is available" do
    let(:config) do
      {
        tag: "redis.slowlog",
        interval: 0,
        logsize: 10
      }
    end

    before do
      allow(plugin).to receive(:redis).and_return(fake_redis)
    end

    after do
      # Wait for the thread polling redis to finish
      plugin.__send__(:watcher).join
    end

    it "does not raise errors" do
      expect { run }.not_to raise_error
    end

    it "polls the slowlog with the configured interval and size" do
      # thread_create startup
      expect(plugin).to receive(:sleep).with(0.1).at_least(:once)
      # core test
      expect(fake_redis).to receive(:slowlog).with("get", 1).ordered
      expect(plugin).to receive(:sleep).with(0).ordered
      expect(fake_redis).to receive(:slowlog).with("get", 10).ordered
      expect(plugin).to receive(:sleep).with(0).ordered
      expect(fake_redis).to receive(:slowlog).with("get", 10).ordered

      # Limit to 2 cycles
      allow(plugin).to receive(:watching).thrice.and_return(true, true, false)

      run
    end

    it "retries when a connection error occurs" do
      # thread_create startup
      expect(plugin).to receive(:sleep).with(0.1).at_least(:once)
      # core test
      expect(fake_redis).to receive(:slowlog).with("get", 1).ordered
      expect(plugin).to receive(:sleep).with(0).ordered
      expect(fake_redis).to receive(:slowlog).with("get", 10).ordered.and_raise(Redis::ConnectionError)
      expect(plugin).to receive(:sleep).with(0).ordered
      expect(fake_redis).to receive(:slowlog).with("get", 10).ordered

      # Limit to 2 cycles
      allow(plugin).to receive(:watching).thrice.and_return(true, true, false)

      run
    end

    context "when the slowlog returns entries", :aggregate_failures do
      let(:startup_slowlog) { [[25637, 1590522108, 5, %w[SCAN 0]]] }
      let(:logged_slowlog) do
        [
          [25640, 1590522258, 1, %w[ping]],
          [25639, 1590522249, 1, %w[ping]],
          [25638, 1590522208, 5, %w[SCAN 0]]
        ]
      end
      let(:slowlog) { logged_slowlog + startup_slowlog }

      let(:expected_entries) do
        logged_slowlog.map(&method(:log_entry)).sort_by { |event| event["id"] }
      end

      let(:emitted_entries) { driver.events.map(&:last) }

      it "emits an event for each slowlog in reverse order" do
        expect(fake_redis).to receive(:slowlog).and_return(startup_slowlog, slowlog)

        run(expect_emits: 3)

        expect(driver.events.size).to eq(3)
        expect(emitted_entries).to eq(expected_entries)
      end

      it "does not log the same event twice" do
        expect(fake_redis).to receive(:slowlog).and_return(startup_slowlog, slowlog.last(2), slowlog)

        run(expect_emits: 3)

        expect(driver.events.size).to eq(3)
        expect(emitted_entries).to eq(expected_entries)
      end

      it "emits results that can be streamed to fluent using MessagePack" do
        expect(fake_redis).to receive(:slowlog).and_return(startup_slowlog, slowlog)

        run(expect_emits: 3)

        expect { driver.events.map(&:to_msgpack) }.not_to raise_error
      end

      context "when redis restarts" do
        let(:logged_slowlog) { [[25638, 1590522208, 5, %w[SCAN 0]]] }
        let(:reset_slowlog) do
          [
            [2, 1590522258, 1, %w[ping]],
            [1, 1590522249, 1, %w[ping]]
          ]
        end

        it "emits all items" do
          reset_entries = reset_slowlog.map(&method(:log_entry)).sort_by { |e| e["id"] }
          expect(fake_redis).to receive(:slowlog).and_return(startup_slowlog, slowlog, [], reset_slowlog)

          run(expect_emits: 3)

          expect(emitted_entries.size).to eq(3)
          expect(emitted_entries).to eq(expected_entries + reset_entries)
        end
      end
    end
  end

  def log_entry(slowlog_entry)
    { "id" => slowlog_entry.first,
      "time" => Time.at(slowlog_entry[1].to_i).utc.iso8601(3),
      "exec_time" => slowlog_entry[2],
      "command" => slowlog_entry.last }
  end
end

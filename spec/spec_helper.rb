require "rspec"
require "fluent/test"
require "fluent/test/driver/input"
require "fluent/test/helpers"
require "pry"

Dir.glob(File.expand_path("../lib/**/*.rb", __dir__)).each(&method(:require))

# Test::Unit is required for fluent/test, but we don't want it to autorun when
# required
Test::Unit::AutoRunner.need_auto_run = false

RSpec.configure do |config|
  config.include Fluent::Test::Helpers
end

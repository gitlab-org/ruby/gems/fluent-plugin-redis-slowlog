# fluent-plugin-redis-slowlog

[Fluentd](https://fluentd.org/) input plugin to write redis-slowlog
events to fluentd.

The plugin works by issuing a
[`SLOWLOG`](https://redis.io/commands/slowlog) command at an
interval. The new slowlog entries will be emitted to fluentd.

The output looks like this:

```
2020-05-26 10:52:27.000000000 +0200 redis.slowlog: {"id":21733,"timestamp":"2020-05-26 10:52:25 +0200","exec_time":5,"command":["SCAN","0"]}
2020-05-26 10:52:37.000000000 +0200 redis.slowlog: {"id":21734,"timestamp":"2020-05-26 10:52:27 +0200","exec_time":197,"command":["slowlog","get","128"]}
2020-05-26 10:52:37.000000000 +0200 redis.slowlog: {"id":21735,"timestamp":"2020-05-26 10:52:32 +0200","exec_time":3,"command":["SET","hello","world"]}
2020-05-26 10:52:47.000000000 +0200 redis.slowlog: {"id":21736,"timestamp":"2020-05-26 10:52:37 +0200","exec_time":259,"command":["slowlog","get","128"]}
2020-05-26 10:52:47.000000000 +0200 redis.slowlog: {"id":21737,"timestamp":"2020-05-26 10:52:42 +0200","exec_time":5,"command":["SCAN","0"]}
```

This is based on the work by @andrewn in
https://github.com/suprememoocow/fluent-plugin-redis-slowlog, which
was forked from https://github.com/mominosin/fluent-plugin-redis-slowlog

## Installation

### RubyGems

```
$ gem install fluent-plugin-redis-slowlog
```

### Bundler

Add following line to your Gemfile:

```ruby
gem "fluent-plugin-redis-slowlog"
```

And then execute:

```
$ bundle
```

## Configuration

### tag (string) (required)

The tag to be used for the events

#### Redis connection parameters

This allows configuring the connection parameters. If nothing is
configured, falling back to the default of localhost:6379.

For more information see the
[`redis-rb`](https://github.com/redis/redis-rb#getting-started).

### url (string) (optional)

A [`redis://`-URL](https://www.iana.org/assignments/uri-schemes/prov/redis).

### path (string) (optional)

Path to connect to Redis listening on a Unix socket

### host (string) (optional)

Host to connect to Redis

Default value: `localhost`.

### port (integer) (optional)

Port to connect to Redis, used in combination with the `host` parameter

Default value: `6379`.

### password (string) (optional)

The password to use for connecting to redis.

Default value: `nil`.

### logsize (integer) (optional)

The number of slowlog entries to get in 1 call.

Default value: `128`.

### interval (integer) (optional)

The time in seconds to wait between `SLOWLOG` commands.

Default value: `10`.

## Development

After checking out the repository, run `bundle install` to install all
dependencies.

After that, you can run `bundle exec rake` to run all lints and
specs.

Other rake tasks:

```
rake build                # Build gitlab-fluent-plugin-redis-slowlog-0.gem into the pkg directory
rake clean                # Remove any temporary products
rake clobber              # Remove any generated files
rake console              # Start an interactive console with the gem loaded
rake install              # Build and install gitlab-fluent-plugin-redis-slowlog-0.gem into system gems
rake install:local        # Build and install gitlab-fluent-plugin-redis-slowlog-0.gem into system gems without network access
rake release[remote]      # Create tag v0 and build and push gitlab-fluent-plugin-redis-slowlog-0.gem to rubygems.org
rake spec                 # Run RSpec code examples
rake verify               # Run RuboCop
rake verify:auto_correct  # Auto-correct RuboCop offenses
```

### Releasing a new version

Releasing a new version can be done by pushing a new tag, or creating
it from the
[interface](https://gitlab.com/gitlab-org/fluent-plugin-redis-slowlog/-/tags).

A new changelog will automatically be added to the release on GitLab.

The new version will automatically be published to
[rubygems](https://rubygems.org/gems/gitlab-fluent-plugin-redis-slowlog)
when the pipeline for the tag completes. It might take a few minutes
before the update is available.
